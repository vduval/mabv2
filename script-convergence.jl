#= MongeAmpereBV2

Copyright 2017 INRIA
Contributors: Jean-David Benamou & Vincent Duval 
jean-david.benamou@inria.fr
vincent.duval@inria.fr

This software is a computer program whose purpose is to compute the
solution to the Monge-Ampere problem.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.

=#

#= This script tests the convergence of the method as the grid step refines =#

push!(LOAD_PATH, "./")
using LinearAlgebra
using Mokaaux

using Superbase
using Source
using Target
using Curves
using MongeAmpere
using PyPlot

#Different variants of the Scheme are defined in the Newton module
# regular: adds U[N0] everywhere
# nonsquare: adds the constraint U[N0] to the scheme (-> non-square matrix)
ntscheme="regular"
if ntscheme=="regular"
  include("Newton.jl") 
elseif ntscheme=="nonsquare" 
  include("Newton_experimental1.jl") 
else
  error("Unknown scheme")
end
using .Newton
verbose=false


####  Computation Domain D=[Dmin[1],Dmax[1]]x[Dmin[2],Dmax[2]]  ########
Dmin=[-1,-1] #coordinates of the lower left-hand corner
Dmax=[1,1] #coordinates of the upper right-hand corner
M,N=64,64 # grid size
xgridh,xgridv=range(Dmin[1],Dmax[1],M),range(Dmin[2],Dmax[2],N) #grid in the D domain
stepxh,stepxv=xgridh[2]-xgridh[1],xgridv[2]-xgridv[1]# Step for finite differences (in the horizontal and vertical directions)
dS=stepxh*stepxv
dS2=dS^2

#### Maximal depth in the SternBroccot tree (infinity-norm of e) #######
maxnorm=3

### Choice of the source ###########
# Choice of the domain: Square
xbd=Curves.genSquare(1,0)
F,GI=Source.genSourceDomain(xbd,xgridh,xgridv)
#Choice of the source density:
# Cosine density as in [BFO14]. Specifically designed for square [-0.5,0.5]^2 target !!
F,D1Uclos,D2Uclos=Source.genCosDensity!(F,xgridh,xgridv)
F[:]/=(sum(F))*dS  

### Choice of the target ###########
# Choice of the target domain:
#Square [-1/2,1/2]x[-1/2,1/2]
target =Curves.genSquare(1,0.) # Boundary of the square
#Choice of the target density: uniform
tdens="uniform" 
g,Dg=Target.setDensity(tdens,target) # Function for the density, and its derivatives


### Computes an initial potential #########
u0=Target.computeInitial(maxnorm,target,xgridh,xgridv,scalelamb=0.8) # Reduce scalelamb if the algorithm gets stuck because of convexity issues
U=u0[:]

#figure()
Mokaaux.plotMapping(U,M,N,stepxh,stepxv,target,true)
title("Initial Map")

#Packing problem description for the algorithm
mopt=Newton.MAOption()
mopt.M,mopt.N=M,N # Size of the computation domain
mopt.maxnorm=maxnorm 
mopt.target=target
mopt.stepxh=stepxh
mopt.stepxv=stepxv
mopt.GI=GI
mopt.F=F
mopt.g=g
mopt.Dg=Dg

#Packing Newton descent options for the algorithm
nopt=Newton.NewtonOption()
nopt.maxnit=100 # Number of iterations
nopt.maxdicho=10 # Maximum number of damping steps
nopt.verbose=true # Verbose mode
nopt.tol=0.0001 # tolerance for convexity defects (INCREASE it if the Newton descent gets stuck because of ``convexity'' issues)
nopt.cvtol=1E-9 # precision for the convergence of the Newton method (if norm(H,2)/(M*N) < cvtol, we say that the method has converged)

#Damped Newton
Newton.newtonsolve!(U,mopt,nopt)

#Plot the resulting map
Mokaaux.plotMapping(U,M,N,stepxh,stepxv,target,false)
title("Computed map")

#For cosine: compare the argument
ggh = zeros(M,N)
ggv = zeros(M,N)
Mokaaux.computeGradient!(ggh,ggv,U,M,N,stepxh,stepxv)
subplot(1,2,1)
Mokaaux.plotGradient(ggh,ggv,M,N,target,false)
title("Computed map")
subplot(1,2,2)
Mokaaux.plotGradient(D1Uclos,D2Uclos,M,N,target,false) 
title("Analytical map")

#Average error in the gradient
I=findall(F.>0) # Only compute the error inside the source domain
println("Average error in the gradient horizontal component:  $(norm(ggh[I]-D1Uclos[I],1)/(norm(D1Uclos[I],1)))")
println("Average error in the gradient vertical component:  $(norm(ggv[I]-D2Uclos[I],1)/(norm(D2Uclos[I],1)))")



