#= MongeAmpereBV2

Copyright 2017 INRIA
Contributors: Jean-David Benamou & Vincent Duval 
jean-david.benamou@inria.fr
vincent.duval@inria.fr

This software is a computer program whose purpose is to compute the
solution to the Monge-Ampere problem.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.

=#

module Curves
#= Routines for manipulating polygonal curves =#


## Standard shapes ##

function genDisc(r=1,nbpoints=100,theta=0)
  #= Constructs a polygonal approximation to a circle.

    y=gen_Disc(r=1,nbpoints=100,theta=0)

    Parameters:
    *r: Radius
    *npoints: number of points to describe the boundary
    *theta: rotation angle (in radian) 

    Returns:
    *y: 2xnbpoints array describing the boundary
      =#
      return genEllipse(r,r,nbpoints,theta)
end


function genEllipse(a=1,b=1,nbpoints=24,theta=0)
  #= Constructs a polygonal approximation to an ellipse.

    y=gen_Ellipse(a=1,b=1,nbpoints=24,theta=0)

    Parameters:
    *a: First half-axis
    *b: Second half-axis
    *npoints: number of points to describe the boundary
    *theta: Rotation angle for the ellipse

    Returns:
    *y: 2xnbpoints array describing the boundary
      =#
      y=zeros(2,nbpoints)
      y[1,:]= a*cos.(2*pi*(0:nbpoints-1)/nbpoints)
      y[2,:]= b*sin.(2*pi*(0:nbpoints-1)/nbpoints)
      c,s=cos(theta),sin(theta)
      R=[c -s;s c]
      return R*y
end

function genSquare(a=1,theta=0)
  #= Constructs a square.
    y,area=genSquare(a=1,theta=0)

    Parameters:
    *a: Side length
    *theta: Rotation angle
    *npoints: number of points to describe the boundary

    Returns:
    *y: 2xnbpoints array describing the boundary
      =#
      y=float([1 -1 -1 1; 1 1 -1 -1])*a/2.
      c,s=cos(theta),sin(theta)
      R=[c -s;s c]
      return R*y
end

function genPacman(r=1,nbpoints=24,phi=pi/6,theta=0.)
      #=
      phi: half angle of the removed slice
      =#
       if nbpoints<=3
         error("genPacman requires nbpoints >= 4")
       end
      y=zeros(2,nbpoints)
      y[:,1]=[0,0]
      angles=range(phi,2*pi-phi,(nbpoints-1))
      y[1,2:end]= r*cos.(angles)
      y[2,2:end]= r*sin.(angles)
      c,s=cos(theta),sin(theta)
      R=[c -s;s c]
      return R*y
end


###### Auxiliary functions

function computeArea(y)
  #= Computes the area of a polygon enclosed by the curve y.
  Parameters:
  * y: 2xN array  containing the coordinates of the N vertices of the polygon, in the trigonometric order.

  Returns:
  * area: area enclosed by the polygon.

  WARNING: this routine is only valid for simple polygons (eg: not the antiparallelogram)
    =#
    area=0.
    showwarn=false
    for i in 1:size(y,2)-1
      val=y[1,i]*y[2,i+1]-y[2,i]*y[1,i+1]
      area+=val
      if val<0
        showwarn=true
      end
    end
    #Last point
    area+=y[1,end]*y[2,1]-y[2,end]*y[1,1]
    if showwarn
      warn("computeArea: One elementary area is negative. Did you really use the trigonometric order?")
    end
    return area/2.
  end


function computeBar(y)
  #= Computes the isobarycenter of a polygon enclosed by the curve y.
  Parameters:
  * y: 2xN array  containing the coordinates of the N vertices of the polygon, in the trigonometric order.

  Returns:
  * yG: area enclosed by the polygon.

  WARNING: this routine is only valid for simple polygons (eg: not the antiparallelogram)
    =#
    area=0.
    showwarn=false
    yG=[0.,0.]
    for i in 1:size(y,2)-1
      val=y[1,i]*y[2,i+1]-y[2,i]*y[1,i+1]
      area+=val
      yG.+=(y[:,i]+y[:,i+1])*val
    end
    #Last point
    val=(y[1,end]*y[2,1]-y[2,end]*y[1,1])
    area+=val
    yG.+=(y[:,end]+y[:,1])*val
    area/=2.
    yG./=6*area
    return yG
  end

function winding(pvec,y)
  #=  
  Computes the winding number so as to test if a point belongs to a polygon 
    (see http://geomalgorithms.com/a03-_inclusion.html#Pseudo-code:%20WN%20pnpoly)
    wn = winding(point,y)

    Parameters:
    *pvec (2xK array): point to test
    *y : (2xn array) boundary of the polygon with n points

    Returns:
    *wn: winding number
    This number is 0 iff the point p is outside the polygon delimited by y
    =#


    function updatewn!(wn,pvec,next,cur)
      #= Updates wn using the winding number algorithm
      wnn=updatewn(wn,p,next,cur)
      Parameters:
      *wn: current winding number
      *pvec: (2xK array) list of K points on which to compute the winding number
      *next,cur: (2x1 arrays) current and next points
      =#

      #Computes the determinant.
      mydet= (next[1]-cur[1])*(pvec[2,:].-cur[2])-(next[2]-cur[2])*(pvec[1,:].-cur[1]) # det(next-cur,p-cur)
      #Points at the left of [cur,next] such that there is an upward crossing
      Ip=findall((mydet.>0) .& (cur[2].<=pvec[2,:]) .& (next[2].>pvec[2,:]))
      wn[Ip].+=1
      #Point at the right of [cur,next] such that there is a downward crossing
      In=findall((mydet.<0) .& (cur[2].>pvec[2,:]) .& (next[2].<=pvec[2,:]))
      wn[In].-=1
    end
    wn=zeros(size(pvec,2))
    n=size(y,2)
    for i=1:n-1 # loop through all edges of the polygon
      updatewn!(wn,pvec,[y[1,i+1],y[2,i+1]],[y[1,i],y[2,i]])
    end
    #Closing the polygon
    updatewn!(wn,pvec,[y[1,1],y[2,1]],[y[1,n],y[2,n]])
    return wn
end




end # end module
