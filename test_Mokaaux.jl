#= MongeAmpereBV2

Copyright 2017 INRIA
Contributors: Jean-David Benamou & Vincent Duval 
jean-david.benamou@inria.fr
vincent.duval@inria.fr

This software is a computer program whose purpose is to compute the
solution to the Monge-Ampere problem.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.

=#

#=
Tests for the mokamkx module

=#
using Revise
push!(LOAD_PATH, "./")


import Mokaaux
mkx=Mokaaux
using PyPlot

function test_shift2ind()
  #Tests the behavior of shift2ind
  M,N=(5,6)
  @assert mkx.shift2ind([0,0],M,N) == 0
  @assert mkx.shift2ind([3,0],M,N) == 3
  @assert mkx.shift2ind([0,2],M,N) == 2*M
  println("shift2ind works well")
end

function test_shift2vec()
  #Tests the behavior of shift2vec
  M,N=(5,6)
  @assert mkx.shift2vec(0,M,N) == [0,0]
  @assert mkx.shift2vec(3,M,N) == [3,0]
  @assert mkx.shift2vec(2*M,M,N) == [0,2]
  println("shift2vec works well")
end

function test_pos2ind()
  #Tests the behavior of pos2ind
  M,N=(5,6)
  @assert mkx.pos2ind([1,1],M,N) == 1
  @assert mkx.pos2ind([3,1],M,N) == 3
  @assert mkx.pos2ind([1,2],M,N) == 1+M
  println("pos2ind works well")
end

function test_pos2vec()
  #Tests the behavior of pos2vec
  M,N=(5,6)
  @assert mkx.pos2vec(1,M,N) == [1,1]
  @assert mkx.pos2vec(3,M,N) == [3,1]
  @assert mkx.pos2vec(1+M,M,N) == [1,2]
  println("pos2vec works well")
end



# Test of the
test_shift2ind()
test_shift2vec()
test_pos2ind()
test_pos2vec()

