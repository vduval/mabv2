#= MongeAmpereBV2

Copyright 2017 INRIA
Contributors: Jean-David Benamou & Vincent Duval 
jean-david.benamou@inria.fr
vincent.duval@inria.fr

This software is a computer program whose purpose is to compute the
solution to the Monge-Ampere problem.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.

=#

#=
Tests for the Curves module

=#
using Revise
push!(LOAD_PATH, "./")
import Curves 

using PyCall
# TODO: remove this line
pygui(:qt5)
using PyPlot
@pyimport matplotlib.patches as patch
@pyimport matplotlib.collections as cl
using LinearAlgebra


function test_winding()
  println("# test_winding")

  fig, ax = subplots()
  patches = [] # list of polygons
  ybd=[0 6 6 3 3 4 4 0;
       0 0 2 2 1 1 3 4] # This is a self intersecting polygon

  polygon = patch.Polygon(ybd', true)
  push!(patches,polygon)
  collecpatch = cl.PatchCollection(patches, cmap="cubehelix", alpha=0.4)
  colors = 100*rand(length(patches))
  collecpatch[:set_array](colors)
  ax[:add_collection](collecpatch)
  xlim([-1,7])
  ylim([-1,7])
  #Test points for the winding number:
  p1=[1,1] # inside the shape -> should be 1
  p2=[3.5,1.5] # in the loop -> should be 2
  p3=[6.5,1] # outside -> should be 0
  p4=[4.5,1.5] # inside the shape -> should be 1

  plot(p1[1],p1[2],"rx",mew=2,label="P1")
  plot(p2[1],p2[2],"bx",mew=2,label="P2")
  plot(p3[1],p3[2],"gx",mew=2,label="P3")
  plot(p4[1],p4[2],"yx",mew=2,label="P4")
  legend()
  pvec=[p1 p2 p3 p4]
  @assert Curves.winding(pvec,ybd) == [1, 2, 0, 1]
  printstyled("-> test of the three cases: OK\n";color=:green)

#plots the map of the winding number
 ngrid=500
 xmin= minimum(pvec[1,:])
 ymin= minimum(pvec[2,:])
 xmax= maximum(pvec[1,:])
 ymax= maximum(pvec[2,:])
 xvec= range(-1,7,ngrid)#(xmin+xmax)/2+ 7*(xmax-xmin)*collect(linspace(-1/2,1/2,ngrid))
 yvec= range(-1,7,ngrid)#(ymin+ymax)/2+ 7*(ymax-ymin)*collect(linspace(-1/2,1/2,ngrid))

 imwn=zeros(ngrid,ngrid)
 bigvec=[Float64[x for x in xvec, y in yvec][:]';Float64[y for x in xvec, y in yvec][:]']
# zeros(2,ngrid^2)
 valwn=Curves.winding(bigvec,ybd)
 ind=1
 for j in 1:ngrid
   for i in 1:ngrid
     imwn[i,j]=valwn[ind]
     ind+=1
   end
 end
 figure()
 imshow(imwn',origin="lower",interpolation="none",extent=[xvec[1], xvec[end], yvec[1], yvec[end]])
 axis("equal")
 colorbar()
 title("Winding Number")
end


function test_computeBar()
  #ybd=[0 1 0;
  #     0 0 1]
  cG=[0.5,0.4]
  N=64
  theta=2*pi*(0:N-1)/N
  ybd= [cos.(theta) sin.(theta)]'
  ybd[1,:].+= cG[1]
  ybd[2,:].+=cG[2]
  #ybd=[0 1 1 0]
  yG=Curves.computeBar(ybd)

  fig, ax = subplots()
  patches = [] # list of polygons
  polygon = patch.Polygon(ybd', true)
  push!(patches,polygon)
  collecpatch = cl.PatchCollection(patches, cmap="cubehelix", alpha=0.4)
  colors = 100*rand(length(patches))
  collecpatch[:set_array](colors)
  ax[:add_collection](collecpatch)
  xlim([-1,7])
  ylim([-1,7])
  plot(yG[1],yG[2],"rx",mew=2,label=L"$y_G$")
  title("Barycenter")
  @assert norm(yG-cG)<1E-10
  printstyled("-> computeBar works well\n";color=:green)
end

test_winding()
test_computeBar()

