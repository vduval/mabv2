#= MongeAmpereBV2

Copyright 2017 INRIA
Contributors: Jean-David Benamou & Vincent Duval 
jean-david.benamou@inria.fr
vincent.duval@inria.fr

This software is a computer program whose purpose is to compute the
solution to the Monge-Ampere problem.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.

=#

module Source 

using Curves

function genSourceDomain(xbd::Array{Float64,2},xgridh,xgridv)
  #=
  Generates a density and a Boolean mask for the source domain X. 

   Parameters:
   *xbd: curve (2xn Float array) for the boundary of the domain X, where n is the number of points
   *xgridh: (1D Float Array):  regular horizontal grid for the domain
   *xgridv: (1D Float Array):  regular vertical grid for the domain

   Returns:  
   *F: (MxN Float array) indicator of the domain (takes values 0 or 1)
   *GI: (MxN Boolean array) boolean mask of the complement of X
  =# 
  (M,N)=length(xgridh),length(xgridv)
  pvec=zeros(2,M*N)
  ind=1
  for b in xgridv
    for a in xgridh
      pvec[:,ind]=[a b]'
      ind+=1
    end
  end
  F=Curves.winding(pvec,xbd) # this returns the winding number
  F=min.(1,(abs.(F)))
  F=reshape(F,(M,N))
  GI=F.<1
  return F,GI
end 

#TODO: faire un genSourceDomain qui prend en charge les trous


function genCosDensity!(F,xgridh,xgridv)
  #= Returns source density with cosine f, and the analytical transport map D1u, D2u
  F: indicator of the support of the source
  xgridh: horizontal grid
  xgridv: vertical grid


  NOTE: Argument F is MODIFIED
  
  =#
PI=float(pi)
q(z)=cos(8*z*PI)*((-1/8)*z^2/PI + (1/256)*PI^(-3) + (1/32)*PI^(-1)) + (1/32)*z*sin(8*z*PI)/PI^2
qprime(z)=(1/32)*sin(8*z*PI)/PI^2 - 8*PI*sin(8*z*PI)*((-1/8)*z^2/PI + (1/256)*PI^(-3) + (1/32)*PI^(-1))
qsecond(z)=(1/4)*cos(8*z*PI)/PI + 2*z*sin(8*z*PI) - 64*PI^2*cos(8*z*PI)*((-1/8)*z^2/PI + (1/256)*PI^(-3) + (1/32)*PI^(-1))

f(x1,x2)= (abs(x1)<0.5 && abs(x2)<0.5) ? 1.0+4.0*(qsecond(x1)*q(x2)+q(x1)*qsecond(x2))+ 16.0*(q(x1)*q(x2)*qsecond(x1)*qsecond(x2)-(qprime(x1)*qprime(x2))^2) : 0.0
D1u(x1,x2)=(abs(x1)<0.5 && abs(x2)<0.5) ? (x1+4*qprime(x1)*q(x2)) : 0.5*x1/max(abs(x1),abs(x2))   
D2u(x1,x2)=(abs(x1)<0.5 && abs(x2)<0.5) ? (x2+4*q(x1)*qprime(x2)) : 0.5*x2/max(abs(x1),abs(x2))


F.*=Float64[ f(xh,xv)  for xh in xgridh, xv in xgridv ]
D1U=Float64[ D1u(xh,xv)  for xh in xgridh, xv in xgridv ]
D2U=Float64[ D2u(xh,xv)  for xh in xgridh, xv in xgridv ]

return F,D1U,D2U
end
end # end module
