#= MongeAmpereBV2

Copyright 2017 INRIA
Contributors: Jean-David Benamou & Vincent Duval 
jean-david.benamou@inria.fr
vincent.duval@inria.fr

This software is a computer program whose purpose is to compute the
solution to the Monge-Ampere problem.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.

=#
using Revise
push!(LOAD_PATH, "./")
using Mokaaux
using Superbase
using Source
using Target
using Curves
using MongeAmpere
using PyPlot

#Different variants of the Scheme are defined in the Newton module
# regular: adds U[N0] everywhere
# nonsquare: adds the constraint U[N0] to the scheme (-> non-square Jacobian matrix)
# replace: replaces one MA equation with the condition U[N0]=0 (-> square Jacobian matrix)
#ntscheme="regular"
ntscheme="regular"
if ntscheme=="regular"
  include("Newton.jl") 
elseif ntscheme=="nonsquare" 
  include("Newton_experimental1.jl") 
#elseif ntscheme=="replace" 
#  include("Newton_experimental2.jl") 
else
  error("Unknown scheme")
end
using .Newton


verbose=false

####  Computation Domain D=[Dmin[1],Dmax[1]]x[Dmin[2],Dmax[2]]  ########
Dmin=[-1,-1] #coordinates of the lower left-hand corner
Dmax=[1,1] #coordinates of the upper right-hand corner
M,N=64,64 # grid size
xgridh,xgridv=range(Dmin[1],Dmax[1],M),range(Dmin[2],Dmax[2],N) #grid in the D domain
stepxh,stepxv=xgridh[2]-xgridh[1],xgridv[2]-xgridv[1]# Step for finite differences (in the horizontal and vertical directions)
dS=stepxh*stepxv
dS2=dS^2

#### Maximal depth in the SternBroccot tree (infinity-norm of e) #######
maxnorm=3

### Choice of the source ###########
# Choice of the domain
schoice="square"
if schoice=="square"
  #Square
  xbd=Curves.genSquare(1,0)
  F,GI=Source.genSourceDomain(xbd,xgridh,xgridv)
elseif schoice=="pacman"
  #Pacman
  xbd=Curves.genPacman(0.7)
  F,GI=Source.genSourceDomain(xbd,xgridh,xgridv)
else 
  error("Unknown Source domain")
end
#Choice of the source density (see also script-convergence.jl for an experiment with the cosine density)
sdens="uniform"
if sdens=="uniform"
  F[:]/=(sum(F))*dS  
elseif sdens=="random"
  F=  F.*(0.01+rand(M,N))  # ones(M,N) ;
  F[:]/=(sum(F))*dS  
else
  error("Unknown Source density")
end

### Choice of the target ###########
# Choice of the target domain
tchoice="disc" #Choice of the target (see below)
if tchoice == "square"   #Square [-1/2,1/2]x[-1/2,1/2]
  target =Curves.genSquare(1,0.) # Boundary of the square
elseif tchoice=="disc"
  target=Curves.genDisc() # Boundary of the unit disc
else 
  error("Unknown target")
end
#Choice of the target density
tdens="uniform" # Choose between uniform, gauss
g,Dg=Target.setDensity(tdens,target) # Function for the density, and its derivatives


### Computes an initial potential #########
u0=Target.computeInitial(maxnorm,target,xgridh,xgridv,scalelamb=0.95) # Reduce scalelamb if the algorithm gets stuck because of convexity issues
U=u0[:]
if verbose
  #Source density
  figure(7)
  imshow(F',cmap="bone",extent=[xgridh[1],xgridh[end],xgridv[1],xgridv[end]],interpolation="none",origin="lower")
  title("Source")
  #Target density
  figure(8)
  gmap=Float64[g([xh,xv]) for xh in xgridh, xv in xgridv]
  pcolormesh(xgridh,xgridv,gmap',cmap="bone",vmin=0.,vmax=maximum(gmap))
  hold("true")
  plot(transpose([target[1,:] target[1,1]]),transpose([target[2,:] target[2,1]]),"r-",linewidth=1) ;
  axis(:equal)
  colorbar()
  title("Target")
  hold("false")
end
Mokaaux.plotMapping(U,M,N,stepxh,stepxv,target,true)
title("Initial Map")

#Packing problem description for the algorithm
mopt=Newton.MAOption()
mopt.M,mopt.N=M,N # Size of the computation domain
mopt.maxnorm=maxnorm 
mopt.target=target
mopt.stepxh=stepxh
mopt.stepxv=stepxv
mopt.GI=GI
mopt.F=F
mopt.g=g
mopt.Dg=Dg

#Packing Newton descent options for the algorithm
nopt=Newton.NewtonOption()
nopt.maxnit=100 # Number of iterations
nopt.maxdicho=10 # Maximum number of damping steps
nopt.verbose=true # Verbose mode
nopt.tol=0.0001 # tolerance for convexity defects (INCREASE it if the Newton descent gets stuck because of ``convexity'' issues,
                # DECREASE if you encounter linear algebra errors)
nopt.cvtol=1E-9 # precision for the convergence of the Newton method (if norm(H,2)/(M*N) < cvtol, we say that the method has converged)

#Damped Newton
Newton.newtonsolve!(U,mopt,nopt)

#Plot the resulting map
Mokaaux.plotMapping(U,M,N,stepxh,stepxv,target,false)
title("Computed map");


#Examine the error map
H=zeros(M*N+1) #matrix of the residual (we aim at cancelling it) 
nsize=17*M*N #at most 12*M*N entries are nonzero in the Jacobian + the trick + rHS 
I,J,V=zeros(Int64,nsize),zeros(Int64,nsize),zeros(Float64,nsize) # Vectors for the contruction of DH
targetscaled =[mopt.stepxh 0; 0 mopt.stepxv]*mopt.target # Rescales the target for the computation of the support function
sptTg=Target.computeSptTarget(mopt.maxnorm,targetscaled) # the support function of the target, evaluated at e,-e,f,-f,g,-g
listS=Superbase.sternBrocot(mopt.maxnorm)
ptr=Newton.computeresidual!(U,H,I,J,V,M,N,sptTg,mopt.maxnorm,listS,mopt.GI,mopt.F,mopt.g,mopt.Dg,dS,dS2,mopt.tdens,mopt.stepxh,mopt.stepxv)

# If regular, add U[N0] again, to 
if ntscheme=="regular"
N0 = round(Int64,0.5*M*N+0.5*N)   
H.+=U[N0]
end
figure()
imshow(reshape(H[1:M*N],(M,N)))
title("Image of the residual")
clim(0,0.06)
colorbar()
