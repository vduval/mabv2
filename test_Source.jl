#= MongeAmpereBV2

Copyright 2017 INRIA
Contributors: Jean-David Benamou & Vincent Duval 
jean-david.benamou@inria.fr
vincent.duval@inria.fr

This software is a computer program whose purpose is to compute the
solution to the Monge-Ampere problem.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.

=#

#=
Tests for the Source module

=#
using Revise
push!(LOAD_PATH, "./")
import Source

using PyPlot


function test_genSourceDomain()
  println("# test_genSourceDomain")
  xbd=Float64[0 6 6 3 3 4 4 0;
  0 0 2 2 1 1 3 4] # This is a self intersecting polygon
  M,N=64,64
  cx=[3.,3.]
  stepxh=0.1
  stepxv=0.1
  xgridh,xgridv= ((-M+1):2:(M-1))*stepxh/2 .+cx[1], ((-N+1):2:(N-1))*stepxv/2 .+cx[2]

  F,GI=Source.genSourceDomain(xbd,xgridh,xgridv)

  figure()
  imshow(F',extent=[xgridh[1],xgridh[end],xgridv[1],xgridv[end]],interpolation="none",origin="lower")
  colorbar()
  title(L"Indicator of domain $X$")
  figure()
  imshow(float.(GI)',extent=[xgridh[1],xgridh[end],xgridv[1],xgridv[end]],interpolation="none",origin="lower")
  title(L"Boolean mask of $X^c$")
  colorbar()
  println("You should see the maps indicator and opposite mask for the self-intersecting polygon")
end

function test_genSourceDomain2()
  println("# test_genSourceDomain2")
  xbd=Curves.genPacman()
  M,N=64,64
  cx=[0.,0.]
  stepxh=0.1
  stepxv=0.1
  xgridh,xgridv= ((-M+1):2:(M-1))*stepxh/2 .+cx[1], ((-N+1):2:(N-1))*stepxv/2 .+cx[2]

  F,GI=Source.genSourceDomain(xbd,xgridh,xgridv)

  figure()
  imshow(F',extent=[xgridh[1],xgridh[end],xgridv[1],xgridv[end]],interpolation="none",origin="lower")
  colorbar()
  title(L"Indicator of domain $X$")
  figure()
  imshow(float.(GI)',extent=[xgridh[1],xgridh[end],xgridv[1],xgridv[end]],interpolation="none",origin="lower")
  title(L"Boolean mask of $X^c$")
  colorbar()
  println("You should see the maps indicator and opposite mask for the self-intersecting polygon")
end

 test_genSourceDomain()
 test_genSourceDomain2()

