# MongeAmpereBV2 (v0.8)
===================================



This Julia toolbox is intended to compute a solution to the Monge-Ampère equation
$$\det(D^2u)=f(x)/g(\nabla u(x)) $$
using the method described in "Minimal convex extensions and finite difference discretization of the quadratic Monge-Kantorovich problem" by Benamou and Duval. A version of the HAL preprint is distributed in the "paper" folder. That paper actually extends the MA-LBR operator introduced in the paper "Monotone and Consistent discretization of the Monge-Ampere operator" by Benamou, Collino and Mirebeau for the use of BV2 boundary conditions. 


## How to
The main program is the script run-MALBR.jl which sets the Optimal Transport problem and calls the Newton solver. It has been tested under Julia 1.8.0. In the Julia REPL, one should simply type:

```julia
include("run-MALBR.jl")
```


The user is invited to modify the choice of source domain (schoice) and density (sdens) as well as the target domain (tchoice) and density (tdens) so as to try different configurations.

Requirements:
This toolbox requires the Julia PyPlot, Revise, PyCall, JuMP and SCS packages.


## Release notes

New in version 0.8.3 (03/10/2022):
- Updated the code so as to run on Julia 1.8.0 (version 0.6 previously).


New in version 0.8.2 (31/05/2018):
- Fixed a bug in the display if a negative Delta value appears


New in version 0.8 (30/05/2018):
- Moved the Cosine density test to the file "Script-convergence.jl"
- Added a variant of the scheme which agrees with the paper (ie, where U[N0]=0 is added as a constraint)

New in version 0.7:
- Works in Julia 0.6.1
- Switched from Mosek to SCS for the intialization

New in version 0.6:
- cosine density
- changed printing options




In the next versions, we plan to:
- add more configurations for source and target density
- add a view of the geodesics
- improve the speed
- wrap the toolbox as a regular Julia package

## Copyright and License
This program is distributed under the CeCILL License (see the LICENSE file).

Copyright 2017 INRIA

Contributors: Jean-David Benamou & Vincent Duval 

jean-david.benamou@inria.fr
vincent.duval@inria.fr

