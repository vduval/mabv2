#= MongeAmpereBV2

Copyright 2017 INRIA
Contributors: Jean-David Benamou & Vincent Duval 
jean-david.benamou@inria.fr
vincent.duval@inria.fr

This software is a computer program whose purpose is to compute the
solution to the Monge-Ampere problem.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.

=#

module Newton
using LinearAlgebra
using SparseArrays
using MongeAmpere
using Target
using Superbase
using Mokaaux

function computeresidual!(U,H,I,J,V,M,N,sptTg,maxnorm,listS,GI,F,g,Dg,dS,dS2,tdens,stepxh,stepxv)
#= Defines the residual with the MA-LBR operator and its degenerate version


  Returns:
  *ptr : indicates the filled size of I,J,V

  Note: this function modifies the H,I,J,V argument
=#
  I[:].=0;J[:].=0;V[:].=0.0 #reinitialize I,J,V which encode the Jacobian DH #TODO update this with the dot notation
  ptr=0
  N0 = round(Int64,0.5*M*N+0.5*N) #position of the point to set to "impose" U[N0]=0
  
  for ind = 1:M*N #For each point in D
    if GI[ind]  # If x is in D\X, use degenerate operator
      D2min,vecinf = MongeAmpere.Compute_Dgnr_Op(U,M,N,ind,sptTg,maxnorm,listS) 
      H[ind] = D2min/dS - U[N0] #Scale by 1/dS to obtain a O(1) quantity
      ptr=MongeAmpere.derivsingleDelta!(I,J,V,ptr,vecinf,M,N,ind,mult=1.0/dS)   # Updates the Jacobian and return the next position ptr
      #Update the Jacobian with the contribution of -U[N0]
      I[ptr+1:ptr+1] .= ind
      J[ptr+1:ptr+1] .= N0 
      V[ptr+1:ptr+1] .= -1.0
      ptr = ptr+1 ; 

    else # Else x is in X, use the regular MongeAmpere operator

      #Evaluate g at the mapped point at \nabla u(x) 
      if tdens=="uniform"
        goGrad=g([0.,0.])
      else
        rose = Mokaaux.nsew(ind,M,N) #Indices North, South, ...
        gradUC = Mokaaux.gradc(rose,U,stepxh,stepxv)  
        goGrad=g(gradUC)
        (Dgh,Dgv)=Dg(gradUC)
        #Inexact Jacobian always non-negative:
        Gn = max(0., F[ind]./goGrad^2*Dgh) 
        Gs = - min(0., F[ind]./goGrad^2*Dgh) 
        Ge = max(0., F[ind]./goGrad^2*Dgv) 
        Gw = - min(0., F[ind]./goGrad^2*Dgv) 
        #Update Jacobian with the second term
        I[ptr+1:ptr+4] .= ind
        J[ptr+1:ptr+4] .= rose 
        V[ptr+1:ptr+4] .= [Gn,Gs,Ge,Gw] 
        ptr = ptr+4 ; 
        I[ptr+1:ptr+1] .= ind
        J[ptr+1:ptr+1] .= ind 
        V[ptr+1:ptr+1] .= -(Gn+Gs+Ge+Gw)
        ptr = ptr+1 ; 
      end
      hmin,mindelta,bests = MongeAmpere.Compute_Op!(U,M,N,ind,maxnorm,listS,1.0) 
      H[ind]=hmin/dS2-F[ind]./(goGrad)  -U[N0]
      ptr=MongeAmpere.derivh!(I,J,V,ptr,U,M,N,ind,bests,alpha=1.0/dS2) #Update Jacobian for hmin/dS2
      #Update the Jacobian with the contribution of -U[N0]
      I[ptr+1:ptr+1] .= ind
      J[ptr+1:ptr+1] .= N0 
      V[ptr+1:ptr+1] .= -1.0
      ptr = ptr+1 ; 
    end #end if (on whether ind is in X or D\X)
  end # end for ind
  return ptr
end

function checkconvex(U,M,N,sptTg,maxnorm,listS,tol=0.)
  #=
  * tol: replaces the test Delta <0. with Delta<(-tol)
  
  Returns true if the function U is convex in the discrete sense, ie Δ_eU>=0 for all irreducible e.
  In fact, we only test the vectors of the superbases such that |e|_∞\leq maxnorm (contrary to the genuine discrete convexity)
  =#
    isdeltaneg,negval=false,0.
    for ind in 1:M*N
      isdeltaneg,negval=MongeAmpere.isDeltaneg(U,M,N,ind,sptTg,maxnorm,listS,tol)
      if isdeltaneg; return false,negval; end
    end
    return true,0.
  end


mutable struct MAOption
  M::Int64 # h size of the domain
  N::Int64 # v size of the domain
  maxnorm::Int64 # depth of the Stern-Brocot tree
  target::Array{Float64,2}
  stepxh::Float64
  stepxv::Float64
  GI::BitArray{2}
  F::Array{Float64,2}
  g::Function
  Dg::Function
  tdens::String
  #MAOption()=new(-1,-1,-1,Array(Float64,0,0),-1.0,-1.0,BitArray(0,0))
  MAOption(x)=new()
end
function MAOption()
  #Initialize with emtpy values
  mopt=MAOption("empty")
  mopt.M=-1
  mopt.N=-1
  mopt.maxnorm=-1
  mopt.target=Array{Float64}(undef,(0,0))
  mopt.stepxh=-1
  mopt.stepxv=-1
  mopt.GI=BitArray(undef,(0,0))
  mopt.F=Array{Float64}(undef,(0,0))
  mopt.tdens="No density"
  return mopt
end


mutable struct NewtonOption  
  maxnit::Int64 # Maximum number of Newton iterations
  maxdicho::Int64 # Maximum of dichotomy steps for damping
  verbose::Bool # Verbose mode (true are false)
  tol::Float64 # Tolerance on the convexity test
  cvtol::Float64 # Tolerance for convergence (error in L2 norm)
  NewtonOption() = new(-1,-1,false,0.,1E-9)
end

function newtonsolve!(U,mopt::MAOption,nopt::NewtonOption)
#= Performs the damped Newton iterations to solve OT.
  Parameters:
  *U (Array{Float64,1}) initial potential. This argument is MODIFIED!
  *mopt: MAOption object
  *nopt: NewtonOpt object

=#

targetscaled =[mopt.stepxh 0; 0 mopt.stepxv]*mopt.target # Rescales the target for the computation of the support function
sptTg=Target.computeSptTarget(mopt.maxnorm,targetscaled) # the support function of the target, evaluated at e,-e,f,-f,g,-g
listS=Superbase.sternBrocot(mopt.maxnorm)
dS=mopt.stepxh*mopt.stepxv
dS2=dS^2
M,N=mopt.M,mopt.N # for brevity


#Variables of the algorithm
Unew=zeros(size(U)) #Iterates
H=zeros(M*N) #matrix of the residual (we aim at cancelling it) 
nsize=17*M*N #at most 12*M*N entries are nonzero in the Jacobian + the trick + rHS 
I,J,V=zeros(Int64,nsize),zeros(Int64,nsize),zeros(Float64,nsize) # Vectors for the contruction of DH

Resinv =zeros(M*N)
for nit in 1:nopt.maxnit
#nit=1
  ptr=Newton.computeresidual!(U,H,I,J,V,M,N,sptTg,mopt.maxnorm,listS,mopt.GI,mopt.F,mopt.g,mopt.Dg,dS,dS2,mopt.tdens,mopt.stepxh,mopt.stepxv) # This updates H,I,J,V
  #NH= norm(H,2) # H is residual 
  #println("##################")
  errL2=norm(H)/(M*N)
  errLinf=norm(H,Inf)
  if nopt.verbose
    println("-> nit=$nit, norm2(H)=$errL2  norminf(H)=$errLinf.  Starting Newton iterations\r")
  end

  if (errL2<nopt.cvtol)
    println("The descent has converged in $nit iterations:  norm2(H)=$errL2   norminf(H)=$errLinf\r")
    break
  end

  # Jacobian and inversion 
  DH=sparse(I[1:ptr],J[1:ptr],V[1:ptr],M*N,M*N) 
  Resinv = DH\H 
  go=true
  dt = 1.0 # Initial Newton step
  safeind=0
  while go && (safeind<nopt.maxdicho)
    #Try an update and check if it is "convex"
    @inbounds @fastmath for ind in 1:M*N; Unew[ind]= U[ind]-dt*Resinv[ind];end
    isconvex,deltaneg=false,0.

    isconvex,deltaneg= Newton.checkconvex(Unew,M,N,sptTg,mopt.maxnorm,listS,nopt.tol)

    if isconvex #If Unew is convex, use this stepsize
      if nopt.verbose
        print("  Testing dt=$dt ... OK"*repeat(" ",80)*"\r") 
      end
      go = false 
    @inbounds @fastmath  for ind in 1:M*N; U[ind]= Unew[ind];end #Copy Unew into U
    else #If some delta is negative, use a smaller step 
      dt = dt/2
      if nopt.verbose
        print("  Testing dt=$dt ... Not OK. Found the value $deltaneg for some Delta_e.  \r") 
      end
    end
    safeind+=1
    if safeind==nopt.maxdicho
      println("Bad news: even with the minimum dt=$dt value, there is a negative Δ_eU: $deltaneg")
    end

  end #end while
end # end for nit
#Show final value 
N0 = round(Int64,0.5*M*N+0.5*N)   
println()
println("Constant U[N_0](should be 0 !)") 
println(U[N0])

end

end # end module


