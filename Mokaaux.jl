#= MongeAmpereBV2

Copyright 2017 INRIA
Contributors: Jean-David Benamou & Vincent Duval 
jean-david.benamou@inria.fr
vincent.duval@inria.fr

This software is a computer program whose purpose is to compute the
solution to the Monge-Ampere problem.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.

=#


module Mokaaux
using PyPlot: plot, axis , scatter, savefig, figure

#= Functions (e.g. for index manipulation and boundary creation)
  REMARK: Julia uses Column-major ordering and indexing starts from 1
=#

export shift2ind, shift2vec, pos2ind, pos2vec, isindomain, plotMapping, plotGeodesic, plotGeodesic_masked, nsew, gradc


function shift2ind(myvec::Array{Int64,1},M::Int64,N::Int64)
  #= Computes the 1D shift corresponding to the vector myvec  =#
  return myvec[1]+(myvec[2])*M
end

function shift2vec(ind::Int64,M::Int64,N::Int64)
  #= Computes the 1D shift corresponding to the vector myvec  =#
  return [ind % M, div(ind,M)]
end

function pos2ind(myvec::Array{Int64,1},M::Int64,N::Int64)
  #= Computes the 1D index corresponding to the vector myvec  =#
  return myvec[1]+(myvec[2]-1)*M
end

function pos2vec(ind::Int64,M::Int64,N::Int64)
  #= Computes the 2D index corresponding to the 1D-position ind  =#
  return [((ind-1)% M)+ 1, div(ind-1,M)+1] 
end

function nsew(ind::Int64,M::Int64,N::Int64)
  #= Returns indices for the (North South...) neighbors
  =#
  (a,b) = pos2vec(ind,M,N) 
  e = pos2ind([a,b+1],M,N) 
  w = pos2ind([a,b-1],M,N) 
  n = pos2ind([a+1,b],M,N) 
  s = pos2ind([a-1,b],M,N) 
  return [n,s,e,w] 
end 

function gradc(ti::Array{Int64,1},U::Array{Float64,1},stepxh::Float64,stepxv::Float64)
  #= ti : Array of indices N.S.E.W.  
  Returns:
  *[dUh,dUv] Centered Finite difference in horizontal and vertical directions
  =# 
  (n,s,e,w) = ti 
  dUh = (U[n]-U[s])/(2*stepxh)
  dUv = (U[e]-U[w])/(2*stepxv)
  return [dUh,dUv]
end 


function isindomain(myvec::Array{Int64,1},M::Int64,N::Int64)
  #= Tells if a position vector myvec is inside the domain =#
  return (0<myvec[1]<M+1) & (0<myvec[2]<N+1)
end

function plotMapping(U::Array{Float64,1},M::Int64,N::Int64,steph::Float64,stepv::Float64,bnd::Array{Float64,2}=Array(Float64,0,0),plotbd::Bool=false)
  #= Plots the gradient map, using centered finite differences (except on the boundary)
  Parameters: 
  *U (Float Array), 
  *M,N
  *steph,stepv: horizontal and vertical stepsizes
  *bnd: target boundary
  *plotbd (=false) if true, plots the non-centered gradient at the boundary (this yields small cells compared to the others)
  =#
  u=reshape(U,M,N)
  ggx = zeros(M,N)
  ggy = zeros(M,N)
  computeGradient!(ggx,ggy,U,M,N,steph,stepv)
  figure()
  plotGradient(ggx,ggy,M,N,bnd,plotbd)
#  figure()
end 

function plotGradient(ggx::Array{Float64,2},ggy::Array{Float64,2},M::Int64,N::Int64,bnd::Array{Float64,2}=Array(Float64,0,0),plotbd::Bool=false)
  #=
  Displays the mapping provided by a gradient (see plotMapping for arguments)
  =#
    if plotbd # Plots with the gradient evaluated on the boundary
      for jj=1:N
        plot(ggx[1:M,jj],ggy[1:M,jj],"b-",linewidth=0.33);    
      end
      for ii=1:M
        plot(ggx[ii,1:N],ggy[ii,1:N],"b-",linewidth=0.33);
      end
    else #Plots without the gradient evaluated on the boundary
      for jj=2:N-1
        plot(ggx[2:M-1,jj],ggy[2:M-1,jj],"b-",linewidth=0.33);    
      end
      for ii=2:M-1
        plot(ggx[ii,2:N-1],ggy[ii,2:N-1],"b-",linewidth=0.33);
      end
    end
    if length(bnd)>0
        plot([bnd[1,:]; bnd[1,1]],[bnd[2,:];bnd[2,1]],"r-",linewidth=1) ;
    end
end

function computeGradient!(ggx::Array{Float64,2},ggy::Array{Float64,2},U::Array{Float64,1},M::Int64,N::Int64,steph::Float64,stepv::Float64)
  #= Computes the gradient by centered finite difference
Modifies ggx,ggy
  ggx, ggy: MxN arrays for the storage of the gradient components
  steph,stepv: grid stepsizes (horizontal and vertical)  

  =#
  u=reshape(U,M,N)
  for jj=1:N
    for ii=2:M-1
      ggx[ii,jj] = (u[ii+1,jj] - u[ii-1,jj])/(2*steph) 
    end
    #For the boundary, use non centered finite differences
    ggx[1,jj] = (u[2,jj] - u[1,jj])/(steph)
    ggx[M,jj] = (u[M,jj] - u[M-1,jj])/(steph)
  end
  for jj=2:N-1
    for ii=1:M
      ggy[ii,jj] = (u[ii,jj+1] - u[ii,jj-1])/(2*stepv) 
    end 
  end
  for ii=1:M
    ggy[ii,1] = (u[ii,2] - u[ii,1])/(stepv) 
    ggy[ii,N] = (u[ii,N] - u[ii,N-1])/(stepv) 
  end 
end


function plotMappingGI(GI::Array{Int64,1} , U::Array{Float64,1},M::Int64,N::Int64,stepx::Float64,stepy::Float64,t::Float64,bnd::Array{Float64,2})


gi = reshape(GI,M,N) 

u=reshape(U,M,N)
ggx = zeros(M,N)
ggy = zeros(M,N)


for ii=2:M-1
    for jj=1:N
        ggx[ii,jj] = (u[ii+1,jj] - u[ii-1,jj])/(2*stepx) 
    end 
end 

for ii=1:M
    for jj=2:N-1
        ggy[ii,jj] = (u[ii,jj+1] - u[ii,jj-1])/(2*stepy) 
    end 
end 


#=
for ii=2:M-1
    ggy[ii,1] = -sqrt(1-ggx[ii,1]^2) 
    ggy[ii,N] = sqrt(1-ggx[ii,N]^2) 
    end 
=#

## interpolation 
   

    for jj=2:M-1
    plot(ggx[2:M-1,jj],ggy[2:M-1,jj],"b-");    
    plot(ggx[jj,2:N-1],ggy[jj,2:N-1],"b-");
   
    end

plot(bnd[1,:]/stepx,bnd[2,:]/stepy,"r-") ;

#savefig("v101a.pdf")

end 



function plotGeodesic(U::Array{Float64,1},M::Int64,N::Int64,stepx::Float64,stepy::Float64,dt::Float64,bnd::Array{Float64,2},v::Array{Float64,1},name::String)

u=reshape(U,M,N)
ggx = zeros(M,N)
ggy = zeros(M,N)

x,y=(1:M)stepx,(1:N)stepy

Y= ones(M)*y'
X = x*ones(N)'


for ii=2:M-1
    for jj=1:N
        ggx[ii,jj] = (u[ii+1,jj] - u[ii-1,jj])/(2*stepx) 
    end 
end 

for ii=1:M
    for jj=2:N-1
        ggy[ii,jj] = (u[ii,jj+1] - u[ii,jj-1])/(2*stepy) 
    end 
end 

#= 
for jj=2:N-1
    ggx[1,jj] = -sqrt(1-ggy[1,jj]^2) 
    ggx[M,jj] = sqrt(1-ggy[M,jj]^2) 
    end 

for ii=2:M-1
    ggy[ii,1] = -sqrt(1-ggx[ii,1]^2) 
    ggy[ii,N] = sqrt(1-ggx[ii,N]^2) 
    end 
=#

## interpolation 


ci = 0 
for t in 0.:dt:1.

ci = ci +1 

gx = (1-t)*X + t*ggx 
gy = (1-t)*Y + t*ggy



    for jj=2:N-1
    plot(gx[2:M-1,jj],gy[2:M-1,jj],"b-");    
end  
 for jj=2:M-1
    plot(gx[jj,2:N-1],gy[jj,2:N-1],"b-");
    end
   

plot(bnd[1,:]/stepx,bnd[2,:]/stepy,"r-") ;

axis(v)  
#axis("equal") 


if (ci < 10) then 
savefig(string(name,"-0",ci,".png")) else 
savefig(string(name,"-",ci,".png")) 
end

sleep(0.5) 
end

end 


function indSrcSpt(f)
  #= Returns the index in the set of points where f!=0
  f: Source density
  =#

  print("TODO")
  end


function plotGeodesic_masked(U::Array{Float64,1},M::Int64,N::Int64,cx::Float64,cy::Float64,stepx::Float64,stepy::Float64,dt::Float64,bnd::Array{Float64,2},MF::Array{Float64,1},v::Array{Float64,1},name::String)

u=reshape(U,M,N)
#F = 1-F 
F = reshape(MF,M,N)
#F = 3*F + 0.1
ggx = zeros(M,N)
ggy = zeros(M,N)

x,y=(1:M)*stepx+cx,(1:N)stepy+cy

Y= ones(M)*y'
X = x*ones(N)'


for ii=2:M-1
    for jj=1:N
        ggx[ii,jj] = (u[ii+1,jj] - u[ii-1,jj])/(2*stepx) 
    end 
end 

for ii=1:M
    for jj=2:N-1
        ggy[ii,jj] = (u[ii,jj+1] - u[ii,jj-1])/(2*stepy) 
    end 
end 

#= 
for jj=2:N-1
    ggx[1,jj] = -sqrt(1-ggy[1,jj]^2) 
    ggx[M,jj] = sqrt(1-ggy[M,jj]^2) 
    end 

for ii=2:M-1
    ggy[ii,1] = -sqrt(1-ggx[ii,1]^2) 
    ggy[ii,N] = sqrt(1-ggx[ii,N]^2) 
    end 
=#

## interpolation 

ci = 0 
for t in 0.:dt:1.



ci = ci+1 

gx = (1-t)*X + t*ggx 
gy = (1-t)*Y + t*ggy


    for jj=2:N-1

#    plot(gx[2:M-1,jj],gy[2:M-1,jj],"b.");    

scatter(gx[2:M-1,jj],gy[2:M-1,jj],F[2:M-1,jj]);    

#    plot(gx[jj,2:N-1],gy[jj,2:N-1],"b.");
#      scatter(gx[jj,2:N-1],gy[jj,2:N-1],"b.");
   
    end
   


plot(bnd[1,:]/stepx,bnd[2,:]/stepy,"r-") ;



#axis("equal") 

if (ci < 10) then 
savefig(string(name,"-0",ci,".png")) else 
savefig(string(name,"-",ci,".png")) 
end


axis(v) 

sleep(0.5) 

end
end 


function plotGeodesic_hole(layer::Float64,h::Array{Float64,2},U::Array{Float64,1},M::Int64,N::Int64,stepx::Float64,stepy::Float64,dt::Float64,bnd::Array{Float64,2},v::Array{Float64,1},name::String)

u=reshape(U,M,N)
ggx = zeros(M,N)
ggy = zeros(M,N)




x,y=(1:M)stepx,(1:N)stepy

Y= ones(M)*y'
X = x*ones(N)'


for ii=2:M-1
    for jj=1:N
        ggx[ii,jj] = (u[ii+1,jj] - u[ii-1,jj])/(2*stepx) 
    end 
end 

for ii=1:M
    for jj=2:N-1
        ggy[ii,jj] = (u[ii,jj+1] - u[ii,jj-1])/(2*stepy) 
    end 
end 

#= 
for jj=2:N-1
    ggx[1,jj] = -sqrt(1-ggy[1,jj]^2) 
    ggx[M,jj] = sqrt(1-ggy[M,jj]^2) 
    end 

for ii=2:M-1
    ggy[ii,1] = -sqrt(1-ggx[ii,1]^2) 
    ggy[ii,N] = sqrt(1-ggx[ii,N]^2) 
    end 
=#

## interpolation 


ci = 0 
for t in 0.:dt:1.

ci = ci +1 

gx = (1-t)*X + t*ggx 
gy = (1-t)*Y + t*ggy


    for jj=2:M-1
    plot(gx[2:M-1,jj],gy[2:M-1,jj],"b-");    
    plot(gx[jj,2:N-1],gy[jj,2:N-1],"b-");
   
    end
   

plot(bnd[1,:]/stepx,bnd[2,:]/stepy,"r-") ;
plot(h[2,:],h[1,:],"g-") ;

ll = [ layer  1.0-layer  1.0-layer  layer  layer ; layer layer 1.0-layer 1.0-layer layer ] ; 
 plot(ll[2,:],ll[1,:],"g-") ;

axis(v)  
axis("equal") 


if (ci < 10) then 
savefig(string(name,"-0",ci,".png")) else 
savefig(string(name,"-",ci,".png")) 
end


sleep(0.5) 


end


end 



end


