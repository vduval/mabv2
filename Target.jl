#= MongeAmpereBV2

Copyright 2017 INRIA
Contributors: Jean-David Benamou & Vincent Duval 
jean-david.benamou@inria.fr
vincent.duval@inria.fr

This software is a computer program whose purpose is to compute the
solution to the Monge-Ampere problem.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.

=#

module Target
#= Routines for specifying the target
  =#
using LinearAlgebra
using Curves
using Mokaaux  
using Superbase
sb=Superbase

using JuMP
using SCS

###### Usual targets ###

function computeSptTarget(maxnorm::Integer,y::Array{Float64,2})
  #= Computes the support function of the target domain delimited by the curve y 
    on the vectors (e, -e, f, -f, g, -g) for each superbase with max(norm(e,Inf))<= maxnorm.

      Returns: 
      sptTg : 6 x (2*maxnorm+1)*(maxnorm) Float Vector
      =#

      listS=Superbase.sternBrocot(maxnorm)
      Nid=(2*maxnorm+1)*(maxnorm)
      sptTg=zeros(6,Nid)
      for s in listS # e[1] varies from -maxnorm to +maxnorm, e[2] from 1 to +maxnorm
        id= sb.etoid(s.e,maxnorm) 
        sptTg[:,id]=SptTarget(float([s.e -s.e s.f -s.f s.g -s.g]),y)
      end
      return sptTg
end

function SptTarget(nu::Array{Float64,2},y::Array{Float64,2})
    #= 
    Evaluates the support function of the target domain delimited by the curve y  on the vectors nu.
    It turns out to be the Legendre-Fenchel transform of the signed distance to a curve.
    It is evaluated according to the formula:

        SptTarget(nu)= sup_y (y.T nu) where y describes the curve
    =#
  maxvec=zeros(Float64,size(nu,2))
  p=0.
  for j in 1:size(nu,2)
    p=nu[1,j]*y[1,1]+nu[2,j]*y[2,1]
    maxvec[j]=p
    for k in 2:size(y,2)
      p=nu[1,j]*y[1,k]+nu[2,j]*y[2,k]
      if p>maxvec[j]
        maxvec[j]=p
      end
    end
  end
  return maxvec
end

#### Densities defined on the target ###

function setDensity(tdens,target)
  #= Define the target density
  Parameters
  *tdens: choice of the density "uniform" or "gauss"
  *target(2xNy Float array): the points of the boundary of Y (real coordinates, not rescaled)

  Returns:
  *g: function which evaluates the density


  We assume that the density is of the form:
  g(y)= 1_Y +psi, 
  where psi is assumed to have compact support (or negligible values near the boundary of Y).

  #TODO: rajouter les calculs de derivees
  =#
  area=Curves.computeArea(target) #Area of Y (in fact we should use the area of Yh...)

  if tdens=="uniform"
    function uniform(y::Array{Float64,1})
      return 1.0/area
    end
    function Duniform(y::Array{Float64,1}) # Derivative
      return 0.,0.
    end
    return uniform,Duniform
  elseif tdens=="gauss" # Density 1_Y+Gaussian entered at yc
    sigh = 0.05
    sigv = 0.05
    yc=[ 0.22, 0.13]
    aA=10. 
    integral=area+aA*2*pi*sigh*sigv # integral of 1_Y + aA*exp(-1/2((yh/sigh)^2+(yv/sigv)^2)
    alpha=1/integral
    beta=aA/integral
    function gauss(y::Array{Float64,1})
      return (alpha+beta*exp(-0.5*(((y[1]-yc[1])/sigh)^2 + ((y[2]-yc[2])/sigv)^2)))
    end
    function Dgauss(y::Array{Float64,1})
      val=beta*exp(-0.5*(((y[1]-yc[1])/sigh)^2 + ((y[2]-yc[2])/sigv)^2))
      return [-(y[1]-yc[1])/sigh^2, -(y[2]-yc[2])/sigv^2]*val
    end
    return gauss,Dgauss
  else
    error("Unknown density choice")
  end
end

######## Auxiliary functions to build densities

## Auxiliary function for the waving target density
q(z)=cos(8*z*pi)*((-1/8)*z^2/pi + (1/256)*pi^(-3) + (1/32)*pi^(-1)) + (1/32)*z*sin(8*z*pi)/pi^2
qprime(z)=(1/32)*sin(8*z*pi)/pi^2 - 8*pi*sin(8*z*pi)*((-1/8)*z^2/pi + (1/256)*pi^(-3) + (1/32)*pi^(-1))
qsecond(z)=(1/4)*cos(8*z*pi)/pi + 2*z*sin(8*z*pi) - 64*pi^2*cos(8*z*pi)*((-1/8)*z^2/pi + (1/256)*pi^(-3) + (1/32)*pi^(-1))


######## Initial potential
function computeInitial(maxnorm::Integer,ybd::Array{Float64,2},xgridh,xgridv;verbose=false,scalelamb=1.0)
  #=
Computes an initial potential u0 which maps [xgridh(1):xgridh(end)] x [xgridv(1): xgridv(end)] into Yh

Parameters:
*maxnorm: maximal norm in the depth of the Stern Brocot tree (determines the approximation Yh of Y)
*ybd:  (2xNy Float array) the points of the boundary of Y (real coordinates, not rescaled)
*xgridh, xgridv: horizontal and vertical grid of the domain D

Returns: 
*u0: potential evaluated at xgridh x xgridv
*scalelamb: For safety margin. Scale the square by scalelamb so as to have strict inclusion in the target. 
  =# 
  #Creates the list of test vectors
  listS=Superbase.sternBrocot(maxnorm)
  nconst=6*length(listS) # number of contraints
  vectab=zeros(2,nconst) # test vectors
  i=1
  for s in listS # e[1] varies from -maxnorm to +maxnorm, e[2] from 1 to +maxnorm
    for vec in Any[s.e, -s.e, s.f, -s.f, s.g, -s.g]
      vectab[:,i]=vec
      i+=1
    end
  end
  #Values of the support function at the test vectors
  sptTg=Target.SptTarget(vectab,ybd)

  #Linear program : find the largest square included in the approximated target Y_h
  #m = Model(solver=MosekSolver())
  m = Model(SCS.Optimizer)
  set_silent(m) # disable verbosity of the solver
  @variable(m, lambda>=0) # size of the square
  @variable(m, c[1:2]) # center of the square
  @constraint(m, constr[i=1:nconst],c[1]*vectab[1,i]+c[2]*vectab[2,i]+lambda*norm(vectab[:,i],1)<= sptTg[i])
  @objective(m, Max, lambda)
  optimize!(m)
  if verbose
    println("### computeInitial: Finding the coordinates of the square inside Yh ###")
    println("Status = $(termination_status(m)) ")
    println("Optimal Objective Function value: $(objective_value(m)) ")
    println("Optimal Solutions:")
    println("lambda = $(value(lambda)) ")
    println("After scaling, using s*lamba = $(scalelamb*value(lambda)) ")
    println("c = $(value(c)) ")
  end
  #Define a quadratic potential whose gradient maps D into Yh
  Lh=(xgridh[end]-xgridh[1])/2.
  Lv=(xgridv[end]-xgridv[1])/2.
  lambdaval=value(lambda)*scalelamb
  cval=value.(c)
  u0=Float64[0.5*lambdaval*((xh/Lh)^2+(xv/Lv)^2)+(cval[1]*xh+cval[2]*xv)    for xh in xgridh, xv in xgridv ]
  return u0
end


######## Plotting Functions

function plotTarget(maxnorm)
#= TODO
=#
     listS=Superbase.sternBrocot(maxnorm)
      listvec=zeros(2,3*length(listS))
      maxmesh=-Inf*ones(size(xmesh))

      for s in listS
        for myvec in Any[s.e, -s.e,  s.f, -s.f,  s.g, -s.g]
          valmesh=(myvec[1]*xmesh + myvec[2]*ymesh - tg.SptTarget(float(reshape(myvec,(2,1))),bdry)[1]).>0
          maxmesh=max(maxmesh,valmesh)
        end
      end
    end



### TODO: eventually remove these functions ####

function SptTargetbis(nu::Array{Float64,2},y::Array{Float64,2})
    #= #DEBUG: to see which is the fastest 
    Evaluates the support function of the target domain delimited by the curve y  on the vectors nu.
    It turns out to be the Legendre-Fenchel transform of the signed distance to a curve.
    It is evaluated according to the formula:

        SptTarget(nu)= sup_y (y.T nu) where y describes the curve
    =#
  maxvec=zeros(Float64,size(nu,2))
  p=nu.T*y
  return maximum(p,2)
end

function computeInitialbar(maxnorm::Integer,y::Array{Float64,2},xgridh,xgridv)
  #=
Computes an initial potential which maps [xvec(1):xvec(end)] x [yvec(1)xyvec(end)] into Yh
using the barycenter
  =# 
  #Barycenter of the target: TODO: incorporer la linalg
  yG=Curves.computeBar(y)
  println("Barycenter $yG")
  ycentered=copy(y)
  @inbounds @simd for i in 1:size(ycentered,2)
     ycentered[:,i]-=yG
  end

  #First, map [-1,1]^2 into Yh
  listS=Superbase.sternBrocot(maxnorm)
  Nid=(2*maxnorm+1)*(maxnorm)
  sptTg=zeros(6,Nid)
  minquot=Inf # minimal value of \sigma_Y(e)/|e|_1

  for s in listS # e[1] varies from -maxnorm to +maxnorm, e[2] from 1 to +maxnorm
    print(s)
    id= sb.etoid(s.e,maxnorm) 
    sptTg[:,id]=SptTarget(float([s.e -s.e s.f -s.f s.g -s.g]),ycentered)
    sptTg[1:2,id]/=norm(s.e,1)
    sptTg[3:4,id]/=norm(s.f,1)
    sptTg[5:6,id]/=norm(s.g,1)
    minquot=min(minquot,minimum(sptTg[:,id]))
  end

  
  #Secondly, rescale so that the potential maps D into Yh
  Lh=(xgridh[end]-xgridh[1])/2.
  Lv=(xgridv[end]-xgridv[1])/2.
  u0= Float64[0.5*minquot*((xh/Lh)^2+(xv/Lv)^2)+(yG[1]*xh+yG[2]*xv)    for xh in xgridh, xv in xgridv ]

  return u0
end


end # end module
