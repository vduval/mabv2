#= MongeAmpereBV2

Copyright 2017 INRIA
Contributors: Jean-David Benamou & Vincent Duval 
jean-david.benamou@inria.fr
vincent.duval@inria.fr

This software is a computer program whose purpose is to compute the
solution to the Monge-Ampere problem.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.

=#
using Revise
push!(LOAD_PATH, "./")
import MongeAmpere
using Mokaaux

using Superbase
ma=MongeAmpere


function test_Compute_Dgnr_Op()
  #TODO write this test
  println("test_Compute_Dgnr_Op not written yet")
end

function test_computeh()
  #Tests the computation of the determinant
  maxnorm=3
  listS=Superbase.sternBrocot(maxnorm)
  sptefg=Inf*ones(6) # we ignore the boundary points
  M,N=30,30
  xvec,yvec=(1:M)/M,(1:N)/N
  dS2=1/(M*N)^2
  ind=pos2ind(round.(Int64,[M/2,N/2]),M,N)
  ntheta=48
  thetavec=(0:ntheta)*pi/ntheta
  kappavec=float(1:12)
  res=zeros(length(thetavec),length(kappavec))
  println("# test_computeh")
  #Build an anisotropic quadratic form Q
  for (i,θ)  in enumerate(thetavec), (j,κ) in enumerate(kappavec) 
    R=[cos(θ) -sin(θ); sin(θ) cos(θ)]
    Q=R*diagm([1/κ, κ])*R'
    u=float([0.5*Q[1,1]*x^2+0.5*Q[2,2]*y^2+Q[1,2]*x*y for x in xvec, y in yvec ])
    U=u[:]

    Dmin=Inf #minimum of h(Δ_e,Δ_f,Δ_g) over all superbases
    bests=listS[1]
    for s in listS
      a,b,c=ma.computeDelta(U,M,N,ind,s,sptefg)
      newD=ma.computeh(max(a,0.),max(b,0.),max(c,0.))
      if newD<Dmin
        Dmin=newD
        bests=s
      end
    end
    Dmin/=dS2
    res[i,j]=Dmin-det(Q)
  end
  #Prepare the plot in polar coordinates in the plane (θ,κ)
  ivec=vec(float([κ*cos(θ) for  θ in thetavec, κ in kappavec]))
  jvec=vec(float([κ*sin(θ) for  θ in thetavec, κ in kappavec]))
  figure()
  contourf(thetavec,kappavec,transpose(res),100)
  title("Approximation error (maxnorm=$maxnorm )")
  xlabel(L"Principal directon $(\theta)$")
  xticks([0,pi/4,pi/2,3pi/4,pi],[0,L"$\pi/4$",L"$\pi/2$",L"$3\pi/4$",L"$\pi$"])
  ylabel(L"Condition number $(\kappa$)")
  colorbar()
  printstyled("-> Displays the approximation error when computing the determinant\n";color=:blue)
end

function test_dirderiv()
  #Tests the directional derivatives
  println("# test_dirderiv")
  M,N=5,6
  u=float([x+y for x in 0:M-1, y in 0:N-1])
  U=u[:]
  fench=38.
  #First test
  posx,posy=2,1
  ind=pos2ind([posx,posy],M,N)
  myvec=[2,1]
  @assert ma.dirderiv(U,ind,posx,posy,myvec,M,N,fench)==3
  #Second test
  myvec=[2,-1]
  @assert ma.dirderiv(U,ind,posx,posy,myvec,M,N,fench)==fench
  #Third test
  posx,posy=M-1,N-1
  ind=pos2ind([posx,posy],M,N)
  myvec=[1,1]
  @assert ma.dirderiv(U,ind,posx,posy,myvec,M,N,fench)==2.
  #Fourth test
  myvec=[2,1]
  @assert ma.dirderiv(U,ind,posx,posy,myvec,M,N,fench)==fench
  printstyled("-> dirderiv works well\n";color=:green)
end

function test_derivsingleDelta()
  #=Tests that the Derivative of \Delta_vec is correct  =#
  M,N=5,6
  u=float([x+y for x in 0:M-1, y in 0:N-1])
  U=u[:]
  nsize=20 # length of vectors I,J,V
  I,J,V=zeros(Int64,nsize),zeros(Int64,nsize),zeros(Float64,nsize)
  ptr=0
  println("# test_derivsingleDelta ")
  #First test : all points (x, x+vec, x-vec) are inside the domain:
  posx,posy=3,2
  ind=pos2ind([posx,posy],M,N)
  myvec=[2,1]
  ptr=ma.derivsingleDelta!(I,J,V,ptr,myvec, M,N,ind;mult=2.)
  @assert  [I[1],I[2],I[3],I[4]]==[8,8,8,8]
  @assert  [J[1],J[2],J[3],J[4]]==[1,8,15,8]
  @assert  [V[1],V[2],V[3],V[4]]==[2,-2,2,-2]
  #Second test:  (x, x+vec) are inside the domain, but x-vec is outside
  posx,posy=1,1
  ind=pos2ind([posx,posy],M,N)
  myvec=[2,1]
  ptr=ma.derivsingleDelta!(I,J,V,ptr,myvec, M,N,ind;mult=3.)
  @assert  [I[5],I[6],I[7],I[8]]==[1,1,0,0]
  @assert  [J[5],J[6],J[7],J[8]]==[8,1,0,0]
  @assert  [V[5],V[6],J[7],J[8]]==[3,-3,0,0]
  #Third test:  (x, x-vec) are inside the domain, but x+vec is outside
  posx,posy=5,6
  ind=pos2ind([posx,posy],M,N)
  myvec=[2,1]
  ptr=ma.derivsingleDelta!(I,J,V,ptr,myvec, M,N,ind;mult=4.)
  @assert  [I[7],I[8],I[9],I[10]]==[30,30,0,0]
  @assert  [J[7],J[8],J[9],J[10]]==[23,30,0,0]
  @assert  [V[7],V[8],J[9],J[10]]==[4,-4,0,0]
  printstyled("-> derivsingleDelta! works well\n";color=:green)
end

function compareDerivh(pos,U,M,N,ind,bests,fench)
    #= Computes the derivative of h by finite differences and using the closed form expression=#
    finstep=0.001
    a,b,c=ma.computeDelta(U,M,N,ind,bests,fench)
    origh=ma.computeh(max(a,0),max(b,0),max(c,0))
    #Finite differences
    finjack=zeros(M*N) #Jacobian (only the ind-th row) estimated by finite differences
    posx,posy=pos2vec(ind,M,N)
    for myvec in Any[bests.e, -bests.e, bests.f, -bests.f, bests.g, -bests.g,[0,0]]
      if !isindomain(pos+myvec,M,N);continue;end
      newU=copy(U)
      newU[pos2ind(pos+myvec,M,N)]+=finstep
      ap,bp,cp=ma.computeDelta(newU,M,N,ind,bests,fench)
      newh=ma.computeh(max(ap,0),max(bp,0),max(cp,0))
      finjack[pos2ind(pos+myvec,M,N)]=(newh-origh)/finstep
    end 
    #Closed form expression
    nsize=20 # length of vectors I,J,V
    I,J,V=zeros(Int64,nsize),zeros(Int64,nsize),zeros(Float64,nsize)
    ptr=0
    ptr=ma.derivh!(I,J,V,ptr,U,M,N,ind,bests)
    indJ=findall(J.!=0)
    #build the matrix 
    closedjack=zeros(M*N)
    for j in indJ
      closedjack[J[j]]+=V[j]
    end
    #Ignore the value 
    println("   Jacobian using closed form $(closedjack[J[indJ]])")
    println("   Jacobian using finite differences form : $(finjack[J[indJ]])")
    if !isempty(indJ)
      @assert(norm(closedjack[J[indJ]]-finjack[J[indJ]],Inf) <1E-2,"The difference between emprical and closed form Jacobian is significant:"*string(norm(closedjack[J[indJ]]-finjack[J[indJ]],Inf)))
    end
  end

function test_derivh_interior()
  #=  Tests the Computation of the Jacobian inside the domain =#
  println("# test_derivh_interior")
    #Compare with the predicted derivative
  M,N=5,6
  #Inside the domain
  posx,posy=3,2
  pos=[posx,posy]
  ind=pos2ind(pos,M,N)
  #y=gen_boundary() # Boundary of a square (UNUSED HERE)
  bests=superbase([2,1],-[1,1],-[1,0]) 
  maxnorm=3
  sptefg=zeros(6)

  ##First test : a>b+c
  U=zeros(M*N)
  U[ind]=0
  U[pos2ind(pos+bests.e,M,N)]=1.5
  U[pos2ind(pos-bests.e,M,N)]=1.5
  U[pos2ind(pos+bests.f,M,N)]=0.5
  U[pos2ind(pos-bests.f,M,N)]=0.5
  U[pos2ind(pos+bests.g,M,N)]=0.5
  U[pos2ind(pos-bests.g,M,N)]=0.5
  println("## Test of the case a> b+c")
  println("  With no zero")
  compareDerivh(pos,U,M,N,ind,bests,sptefg)
  println("  With one zero") #Same but one is zero
  U[pos2ind(pos+bests.g,M,N)]=-0.5
  U[pos2ind(pos-bests.g,M,N)]=-0.5
  compareDerivh(pos,U,M,N,ind,bests,sptefg)

  ##Second test: b> a+c
  U=zeros(M*N)
  U[ind]=0
  U[pos2ind(pos+bests.e,M,N)]=0.5
  U[pos2ind(pos-bests.e,M,N)]=0.5
  U[pos2ind(pos+bests.f,M,N)]=3.5
  U[pos2ind(pos-bests.f,M,N)]=3.5
  U[pos2ind(pos+bests.g,M,N)]=1.5
  U[pos2ind(pos-bests.g,M,N)]=1.5
  println("## Test of the case b> a+c")
  println("  With no zero")
  compareDerivh(pos,U,M,N,ind,bests,sptefg)
  println("  With one zero")#Same but one is zero
  U[pos2ind(pos+bests.g,M,N)]=-0.5
  U[pos2ind(pos-bests.g,M,N)]=-0.5
  compareDerivh(pos,U,M,N,ind,bests,sptefg)

  #Third test: c> a+b
  U=zeros(M*N)
  U[ind]=0
  U[pos2ind(pos+bests.e,M,N)]=0.5
  U[pos2ind(pos-bests.e,M,N)]=0.5
  U[pos2ind(pos+bests.f,M,N)]=1.
  U[pos2ind(pos-bests.f,M,N)]=1.
  U[pos2ind(pos+bests.g,M,N)]=2
  U[pos2ind(pos-bests.g,M,N)]=2
  println("## Test of the case c> a+b")
  println("  With no zero")
  compareDerivh(pos,U,M,N,ind,bests,sptefg)
  println("  With one zero")#Same but one is zero
  U[pos2ind(pos+bests.e,M,N)]=-0.5
  U[pos2ind(pos-bests.e,M,N)]=-0.5
  compareDerivh(pos,U,M,N,ind,bests,sptefg)

  #Fourth test: None
  U=zeros(M*N)
  U[ind]=0
  U[pos2ind(pos+bests.e,M,N)]=0.01
  U[pos2ind(pos-bests.e,M,N)]=0.01
  U[pos2ind(pos+bests.f,M,N)]=0.02
  U[pos2ind(pos-bests.f,M,N)]=0.02
  U[pos2ind(pos+bests.g,M,N)]=0.025
  U[pos2ind(pos-bests.g,M,N)]=0.025
  println("## Test of the 'otherwise' case ")
  println("  With no zero")
  compareDerivh(pos,U,M,N,ind,bests,sptefg)
  #Same but one is zero
  println("  With one zero")
  U[pos2ind(pos+bests.e,M,N)]=-0.1
  U[pos2ind(pos-bests.e,M,N)]=-0.1
  compareDerivh(pos,U,M,N,ind,bests,sptefg)
  printstyled("-> derivh! works well in the interior\n";color=:green)
end





test_computeh()
test_dirderiv()
test_derivsingleDelta()
test_derivh_interior()
#test_derivh_boundary()

