#= MongeAmpereBV2

Copyright 2017 INRIA
Contributors: Jean-David Benamou & Vincent Duval 
jean-david.benamou@inria.fr
vincent.duval@inria.fr

This software is a computer program whose purpose is to compute the
solution to the Monge-Ampere problem.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.

=#

module MongeAmpere

using Superbase
using Mokaaux
#= Functions to compute the Monge-Ampere operator (and its extension in D\X) =#



function Compute_Dgnr_Op(U::Array{Float64,1},M::Int64,N::Int64,ind::Int64,sptTg::Array{Float64,2},maxnorm::Int64,listS::Array{superbase,1})
  #= Computes the Degenerate version of the Monge-Ampere operator (for D\X):
    inf_{e irreducible} δ_{e}U  +δ_{-e}U  
    Where the finite difference  δ_{±e}U is replaced with the support function σ_Y(±e) if x+e is not in D.
        
    Parameters:
    *U
    *M,N: size of the computation domain
    *ind: position (linear index) of the point to compute MA
    *sptTg[vec,n] 6x(2maxnorm+1)*maxnorm: support function of the (rescaled by dxh and dxv) target, evaluated at vec in s.e -s.e s.f -s.f s.g -s.g, for the superbase indexed at n
    *maxnorm:
    *listS: list of the superbases to test

    Returns:
    D2min: Value of the infimum
    vecinf: optimal vector

    COMMENT/TODO: this function could be accelerated by using that the vector e of some superbase is the vector f (or g) of its children...
  =#
  D2min=Inf #minimum of Δ_e,Δ_f,Δ_g over all superbases
  vecinf = [ 0., 0. ] 
  for s in listS
    sptefg=vec(sptTg[:,etoid(s.e,maxnorm)])
    a,b,c=computeDelta(U,M,N,ind,s,sptefg) 
    if a <= D2min 
      D2min = a
      vecinf = s.e
    end 
    if b <= D2min 
      D2min = b
      vecinf = s.f
    end 
    if c <= D2min 
      D2min = c
      vecinf = s.g
    end 
  end
  return D2min,vecinf
end 

function Compute_Op!(U::Array{Float64,1},M::Int64,N::Int64,ind::Int64,maxnorm::Int64,listS::Array{superbase,1},mindelta::Float64)
#=
Computes the Monge-Ampere operator using the MA operator introduced by Benamou, Collino, Mirebeau.

  Parameters:
  *U: 
  *M,N: size of the computation domain
  *ind: position at which to compute MA (should be far from the boundary, i.e. at least at distance maxnorm)
  *maxnorm: maximal norm in the depth of the Stern Brocot tree (determines the approximation Yh of Y)
  *listS:  list of the superbases to test
  *mindelta: minimum value of Delta along (for testing convexity)
  =#

  minh=Inf #minimum of h(Δ_e,Δ_f,Δ_g) over all superbases
  bests=listS[1] # best superbase
  sptTg=zeros(6) # this value is not used since we are far from the boundary
  for s in listS
    a,b,c,=computeDelta(U,M,N,ind,s,sptTg)
    newD=computeh(max(a,0.),max(b,0.),max(c,0.))
    if newD<minh
      minh=newD
      bests=s 
    end
  end                     
  a,b,c=computeDelta(U,M,N,ind,bests,sptTg) 
  mindelta = min(mindelta,a,b,c)    # Updates the minimum value of Delta (for testing convexity later)
  return minh,mindelta,bests
end

### Elementary functions ###
function computeDelta(U::Array{Float64,1},M::Int64,N::Int64,ind::Int64,s::superbase,sptefg::Array{Float64,1})
  #= Computes Δ_e u, Δ_f u, Δ_g u
  Parameters:
  U: function (flattened) on which to compute h
    ind: index at which h should be computed
    s: superbase (e,f,g)
    sptefg: the support function of Y evaluated at [s.e,-s.e,s.f,-s.f,s.g,-s.g]
      =#
      posx,posy=pos2vec(ind,M,N) #position
      a,b,c=0.,0.,0.
      # a = D_{+e}u + D_{-e}u
      a+=dirderiv(U,ind,posx,posy,s.e,M,N,sptefg[1])
      a+=dirderiv(U,ind,posx,posy,-s.e,M,N,sptefg[2])
      #b = D_{+f}u + D_{-f}u
      b+=dirderiv(U,ind,posx,posy,s.f,M,N,sptefg[3])
      b+=dirderiv(U,ind,posx,posy,-s.f,M,N,sptefg[4])
      #c = D_{+g}u + D_{-g}u
      c+=dirderiv(U,ind,posx,posy,s.g,M,N,sptefg[5])
      c+=dirderiv(U,ind,posx,posy,-s.g,M,N,sptefg[6])
      return a,b,c
end

function derivsingleDelta!(I::Array{Int64,1},J::Array{Int64,1},V::Array{Float64,1},ptr::Int64,avec, M,N,ind::Integer; mult::Float64=1.)
  #= Computes the derivative of Δ_avec^+(u) where avec is a vector =#
  posx,posy=pos2vec(ind,M,N) #position
  for myvec in Any[-avec,avec]
    #print(myvec)
    ishift= ind + shift2ind(myvec,M,N)
    if isindomain([posx,posy]+myvec,M,N) 
      #The contribution to Δ_vec^+(u)[ind] is U[ind+shift]-U[ind]
      ptr+=1
      I[ptr],J[ptr],V[ptr]=ind,ishift, mult
      ptr+=1
      I[ptr],J[ptr],V[ptr]=ind,ind,-mult
    end
  end
  return ptr # ptr is Immutable therefore we return it so as to update its value
end


function dirderiv(U::Array{Float64,1},ind::Int64,posx::Int64,posy::Int64,myvec::Array{Int64,1},M::Int64,N::Int64,sptval::Float64)
  #= Returns the directional derivative if both points are inside the domain,
   or the support function evaluated at the corresponding shift otherwise.
   =#
   if isindomain([posx,posy]+myvec,M,N)
     #if (M+1>posx+myvec[1]>0) && (N+1>posy+myvec[2]>0) #TODO: replace this line with isindomain
     shift=shift2ind(myvec,M,N)
     return U[ind+shift]-U[ind]
   else
     return sptval
   end
end


function computeh(a::Float64,b::Float64,c::Float64)
  #= Computes the function h(a,b,c) for the MA-operator
      =#
      h=0.
      if a>= b+c
        h=b*c
      elseif b>= a+c
        h=a*c
      elseif c>= a+b
        h=a*b
      else
        h=0.5*(a*b+b*c+c*a) - 0.25*(a^2+b^2+c^2)
      end
      return h
end

function derivh!(I::Array{Int64,1},J::Array{Int64,1},V::Array{Float64,1},ptr::Int64,U::Array{Float64,1},M::Int64,N::Int64,ind::Int64,bests::Superbase.superbase;alpha=1.0)
  #= Computes the Jacobian DH of the function H(u)=alpha*h(Δ_e^+(u),Δ_f^+(u),Δ_g^+(u)) at ind
    Parameters:
    *I,J,V: Float arrays such that DH[I[k], J[k]] = V[k]. Those arguments are MODIFIED
    *ptr: Integer which indicates the current position in I,J,V. That argument is MODIFIED

    *U: function (flattened) on which to compute
    *M,N: dimensions of (unflattened) U
    *ind: index at which the algorithm should be computed (this function should only be used far from the boundary of the computation domain, i.e. at least at distance maxnorm from the boundary)
    *bests: superbase (e,f,g) which attains the minimum of h(Δ_e^+(u),Δ_f^+(u),Δ_g^+(u)).
    *alpha (=1.0) scaling factor (to compute the Jacobian of alpha*h instead of h)

    Returns:
    ptr: Updated version of ptr 

    COMMENT: let T(x)=max(0,x), so that Δ_e^+=T(Δ_e). Then 
        DH = ∂_1h dT d(Δ_e)+ ∂_2h dT d(Δ_f)+ ∂_3h dT d(Δ_g)
        with dT(x)= 1 if x>0,
                    0 otherwise.

   =#
   sptefg=zeros(6)
   a,b,c=computeDelta(U,M,N,ind,bests,sptefg)
   a*=alpha
   b*=alpha
   c*=alpha
   contribe,contribf,contribg=true,true,true # encodes dT(a),dT(b),dT(c) 
   if (a<0.); a=0.; contribe=false;end
   if (b<0.); b=0.; contribf=false;end
   if (c<0.); c=0.; contribg=false;end

   if (a>=b+c)
     #println("Case A")
     if contribf & contribg # h=b*c hence dh= c dT db + b dT dc
       ptr=derivsingleDelta!(I,J,V,ptr,bests.f, M,N,ind, mult=c) 
       ptr=derivsingleDelta!(I,J,V,ptr,bests.g, M,N,ind, mult=b)
     end
   elseif (b>=a+c)
     #println("Case B")
     if contribe & contribg # h=a*c hence dh= c dT da + a dT dc
       ptr=derivsingleDelta!(I,J,V,ptr,bests.e, M,N,ind, mult=c)
       ptr=derivsingleDelta!(I,J,V,ptr,bests.g, M,N,ind, mult=a)
     end
   elseif (c>= a+b)
     #println("Case C")
     if contribe & contribf# h=a*b hence dh= b dT b da + a dT db
       ptr=derivsingleDelta!(I,J,V,ptr,bests.e, M,N,ind, mult=b)
       ptr=derivsingleDelta!(I,J,V,ptr,bests.f, M,N,ind, mult=a)
     end
   else # h=0.5*(a*b+b*c+c*a) - 0.25*(a^2+b^2+c^2) hence dh= 0.5(b+c-a)dT da +0.5(a+c-b)dT db + 0.5(a+b-c)dT dc
     #println("Case D")
     if contribe; ptr=derivsingleDelta!(I,J,V,ptr,bests.e, M,N,ind,mult=0.5(b+c-a));end
     if contribf; ptr=derivsingleDelta!(I,J,V,ptr,bests.f, M,N,ind, mult=0.5(a+c-b));end
     if contribg; ptr=derivsingleDelta!(I,J,V,ptr,bests.g, M,N,ind, mult=0.5(a+b-c));end
   end
   return ptr
 end

function isDeltaneg(U::Array{Float64,1},M::Int64,N::Int64,ind::Int64,sptTg::Array{Float64,2},maxnorm::Int64,listS::Array{superbase,1},tol=0.)
  #=Returns true if some Delta_e is <0, else returns false.

    Parameters:
    *U
    *M,N: size of the computation domain
    *ind: position (linear index) of the point to compute MA
    *sptTg[vec,n] 6x(2maxnorm+1)*maxnorm: support function of the (rescaled by dxh and dxv) target, evaluated at vec in s.e -s.e s.f -s.f s.g -s.g, for the superbase indexed at n
    *maxnorm:
    *listS: list of the superbases to test
    *tol: replace the test Delta<0 with Delta<-tol

    Returns:
    deltaneg: true or false
    deltafound: found negative value 

    COMMENT/TODO: this function could be accelerated by using that the vector e of some superbase is the vector f (or g) of its children...

  =#
  for s in listS
    sptefg=vec(sptTg[:,etoid(s.e,maxnorm)])
    a,b,c=computeDelta(U,M,N,ind,s,sptefg) 
    if a<(-tol)  
      return true, a
    elseif b<(-tol)
      return true, b
    elseif c<(-tol)
      return true, c
    end
  end
  return false, 0.
end 

end #end module
