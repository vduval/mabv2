#= MongeAmpereBV2

Copyright 2017 INRIA
Contributors: Jean-David Benamou & Vincent Duval 
jean-david.benamou@inria.fr
vincent.duval@inria.fr

This software is a computer program whose purpose is to compute the
solution to the Monge-Ampere problem.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.

=#

module Superbase
using LinearAlgebra
using Mokaaux
#= Julia Code for Monge-Ampere Equation
=#
export superbase,  etoid, idtoe
#= Superbase class  =#
struct superbase
    e::Array{Int64,1}
    f::Array{Int64,1}
    g::Array{Int64,1}

    function superbase(e,f,g)
      #= Basic constructor=#
      @assert !any(map(Bool,e+f+g)) # The sum e+f+g must be zero
      @assert abs(round(Int,det([f g])))==1  ("The determinant of f,g="*string(f)*string(g)*"is"*string(det([f g])))# The determinant det(g,f) must be equal to 1 in absolute value
      new(vec(e),vec(f),vec(g))
    end
end

### Indexing functions ###

function etoid(myvec::Array{Int64,1},m)
  #= The id is a unique index which identifies the superbase, so as to store its attributes in a matrix.
  This function returns the unique id of the superbase with e vector equal to myvec.
    Parameters:
      myvec : "e" vector of the superbase
      m: maxnorm

    Returns:
       id: the id of the superbase 
  =#
  return (myvec[1]+m+1) + (myvec[2]-1)*(2*m+1)
end

function idtoe(id::Int64,m)
  #= The id is a unique index which identifies the superbase, so as to store its attributes in a matrix.
  This function returns the unique vector e with id  e.
    Parameters:
    id: the id of the superbase 
    m: maxnorm

    Returns:
    myvec : "e" vector of the superbase
  =#
   return  [((id -m-1)%(2*m+1)), div((id-1),2*m+1)+1 ]
end



function sternBrocot(maxnorm::Integer)
  #= listS=sternBrocot(maxnorm::Integer)

  Implements the crossing of the SternBrocot tree to generate all superbases until norm(e)=maxnorm

  Returns:
  listS: the list of all the superbases
  =#
  f=[1, 0]
  listG=Array{Float64,1}[[-1,0],[0,1]]
  listS=superbase[]
  
  for counter in 1:1000 # Precaution: limit the number of loops in case of bug
    if isempty(listG); break; end
    g=listG[end]
    e=f+g
    if norm(e,Inf)<=maxnorm
      s=superbase(e,-f,-g)
      push!(listS,s)
      push!(listG,e)
    else
       f=pop!(listG)
    end
  end
  return listS
end





end # end module
