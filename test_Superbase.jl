#= MongeAmpereBV2

Copyright 2017 INRIA
Contributors: Jean-David Benamou & Vincent Duval 
jean-david.benamou@inria.fr
vincent.duval@inria.fr

This software is a computer program whose purpose is to compute the
solution to the Monge-Ampere problem.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.

=#

#=
Tests for the Superbase module

=#
using Revise
push!(LOAD_PATH, "./")

using Mokaaux
import Superbase
sb=Superbase

using PyPlot

function test_superbase()
  #= Tests the constructor for superbase
  =#
  # Good superbase
  e,f,g=[1,1],[-1,0],[0,-1]
  t=sb.superbase(e,f,g) # Validate the disc domain
  # Wrong sum
  e,f,g=[1,1],[-1,0],[0,-2]
  iserror=false
  println("# test_superbase")
  iserror=true
  try
    t=sb.superbase(e,f,g) # Validate the disc domain
  catch
    iserror=false
  end  
  if iserror; error("superbase should complain about the wrong sum");end 
  # Wrong determinant
  e,f,g=[2,2],[0,-2],[-2,0]
  iserror=true
  try
    t=sb.superbase(e,f,g) # Validate the disc domain
  catch 
    iserror=false
  end  
  if iserror; error("superbase should complain about the wrong determinant"); end
  println("-> superbase works well")
end 

function test_etoid()
  println("# test_etoid")
  maxnorm=5
  listS=sb.sternBrocot(maxnorm)
  Nid=(2*maxnorm+1)*(maxnorm)
  fench=zeros(Nid)
  for s in listS # e[1] varies from -maxnorm to +maxnorm, e[2] from 1 to +maxnorm
    id= sb.etoid(s.e,maxnorm) 
    fench[id]=1
  end
  @assert sum(fench) == length(listS)
  printstyled("-> etoid works well\n";color=:green)
end

function test_idtoe()
  println("# test_idtoe")
  maxnorm=7
  for e in Any[[1,1],[1,2]]
    id=sb.etoid(e,maxnorm)
    ecal=sb.idtoe(id,maxnorm)
    @assert ecal == e
  end
  printstyled("-> idtoe works well\n";color=:green)
end

function test_sternBrocot()
  #Tests the Stern-Broccot tree
  maxnorm=4
  listS=sb.sternBrocot(maxnorm)
  l=length(listS)
  X,Y=zeros(l),zeros(l)
  U=Float64[s.e[1] for s in listS]
  V=Float64[s.e[2] for s in listS]
  println("# test_sternBrocot")
  figure() 
  ax = PyPlot.axes()
  #hold(true)
  scatter(U,V)
  ax[:set_xticks](-maxnorm:maxnorm)
  ax[:set_yticks](-maxnorm:maxnorm)
  grid()
  axhline(0, color="black")
  axvline(0, color="black")
  title("Stern-Brocot")
  println("-> If you see the set of vectors e of Stern-Brocot, sternBrocot works well")
end



test_superbase()
test_etoid()
test_idtoe()
test_sternBrocot()
